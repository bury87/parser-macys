# coding: utf8
import scrapy
import re
import json
import csv
import sys
from macys.items import MacysItem

class MacysSpider(scrapy.Spider):
    name = "macys"


    def start_requests(self):
        url = 'http://www1.macys.com/'
        yield scrapy.Request(url=url, callback=self.parse)


    def parse(self, response):
        urls = response.xpath(".//div[@id='globalMastheadCategoryMenu']/ul/li/a/@href").extract()
        for url in urls:
            yield scrapy.Request(url=response.urljoin(url), callback=self.parse1)

    def listappend(self, list1, list2):
        all = []
        for item in list1:
            all.append(item)
        for item in list2:
            all.append(item)
        return all

    def parse1(self, response):
        mainCat = response.xpath(".//div/ul/li[@class='globalMastheadCategorySelected']/a/text()").extract_first()

        urls1 = response.xpath(".//div[@id='localNavigationContainer']/ul/li/a/@href").extract()
        urls2 = response.xpath(".//div[@id='localNavigationContainer']/ul/li/ul/li/a/@href").extract()
        urls = self.listappend(urls1,urls2)
        for url in urls:
            yield scrapy.Request(url=response.urljoin(url), callback=self.parse2, meta={'mainCat': mainCat})


    def parse2(self, response):
        urls1 = response.xpath(".//div[@id='localNavigationContainer']/ul/li/a/@href").extract()
        urls2 = response.xpath(".//div[@id='localNavigationContainer']/ul/li/ul/li/a/@href").extract()
        urls = self.listappend(urls1, urls2)
        for url in urls:
            yield scrapy.Request(url=response.urljoin(url), callback=self.parse3, meta={'mainCat': response.meta['mainCat']})

    def parse3(self, response):

        urls1 = response.xpath(".//div[@id='localNavigationContainer']/ul/li/a/@href").extract()
        urls2 = response.xpath(".//div[@id='localNavigationContainer']/ul/li/ul/li/a/@href").extract()
        urls = self.listappend(urls1, urls2)
        if len(urls)==0:
            idPatern = re.compile(r'\?id=(\d+)')
            idCategory = idPatern.search(response.url)
            if idCategory:
                urlApi = 'http://www1.macys.com/api/navigation/categories/facet?categoryId=' + str(
                    idCategory.group(1)) + '&facet=false&productsPerPage=120&pageIndex=1&kwsNewUrlPattern=true'
                yield scrapy.Request(url=urlApi, callback=self.parse5,
                                     meta={'page': 1, 'category': str(idCategory.group(1)),
                                           'mainCat': response.meta['mainCat']})
        for url in urls:
            yield scrapy.Request(url=response.urljoin(url), callback=self.parse4, meta={'mainCat': response.meta['mainCat']})

    def parse4(self, response):
        idPatern = re.compile(r'\?id=(\d+)')
        idCategory = idPatern.search(response.url)
        if idCategory:
            urlApi = 'http://www1.macys.com/api/navigation/categories/facet?categoryId=' + str(idCategory.group(1)) + '&facet=false&productsPerPage=120&pageIndex=1&kwsNewUrlPattern=true'
            yield scrapy.Request(url=urlApi, callback=self.parse5, meta={'page':1, 'category':str(idCategory.group(1)), 'mainCat':response.meta['mainCat']})



    def parse5(self, response):
        jsonResonse = json.loads(response.body)
        if jsonResonse['FacetResponse']['meta']['productCount'] is not 0:
            productIds = jsonResonse['FacetResponse']['meta']['productIds']
            productCount = jsonResonse['FacetResponse']['meta']['productCount']
            categoryTitle = jsonResonse['FacetResponse']['meta']['facetTitle']
            page = response.meta['page']
            if page * 120 < productCount:
                urlApi = 'http://www1.macys.com/api/navigation/categories/facet?categoryId=' + response.meta['category'] + '&facet=false&productsPerPage=120&pageIndex='+str(page+1)+'&kwsNewUrlPattern=true'
                yield scrapy.Request(url=urlApi, callback=self.parse5, meta={'page': page+1, 'category':response.meta['category'], 'mainCat':response.meta['mainCat']})
            url = self.make_url(productIds, response.meta['category'])
            yield scrapy.Request(url=url, callback=self.parse6, meta={'ids':productIds, 'categoryTitle': categoryTitle, 'mainCat':response.meta['mainCat']})

    def parse6(self, response):
        for id in response.meta['ids']:
            product = MacysItem()
            product['id'] = str(id).replace('"','&#34;').replace(',','&#44;')
            product['url'] = str(response.xpath(".//li[@id='"+ str(id) +"']//div[@class='shortDescription']/a/@href").extract_first()).replace('"','&#34;').replace(',','&#44;')
            product['img'] = str(response.xpath(".//li[@id='"+ str(id) +"']//img[contains(@class,'thumbnailImage')]/@data-src").extract_first()).replace('"','&#34;').replace(',','&#44;')
            name  = response.xpath(".//li[@id='" + str(id) + "']//div[@class='shortDescription']/a/text()").extract()
            product['name'] = ''.join(name).replace('"','&#34;').replace(',','&#44;')
            product['price'] = str(response.xpath(".//li[@id='" + str(id) + "']//meta[@itemprop='price']/@content").extract_first()).replace('"','&#34;').replace(',','&#44;')
            product['mainCategory'] = str(response.meta['mainCat']).replace('"','&#34;').replace(',','&#44;')
            product['subCategory'] = str(response.meta['categoryTitle']).replace('"','&#34;').replace(',','&#44;')
            yield product


    def make_url(self, ids, category):
        ids_with_cat = []
        for id in ids:
            ids_with_cat.append(str(category)+'_'+str(id))
        ids_str = ','.join(ids_with_cat)
        url = 'http://www1.macys.com/shop/catalog/product/thumbnail/1?edge=hybrid&wid=207&limit=none&suppressColorSwatches=false&categoryId='+str(category)+'&ids='+str(ids_str)
        return url
















